<!DOCTYPE html>
<html>

<body>
        <header>
                <h1 class="site-heading text-center text-white d-none d-lg-block">
      <span class="site-heading-upper text-primary mb-3">A Free publication website for researchers</span>
      <span class="site-heading-lower">DROS</span>
    </h1>
                <?php  
      if (isset($_GET) && isset($_GET['username'])){
        echo '<form method="POST"> 
        
        <input type="submit" align="right" value="Log out" name="logout"></input> 

        </form>';
      }
    ?>
                
                <center>
                <div class="recherche">
                  <form method="POST">
                
                  <input type="text" name="recherche" placeholder="Tapez une publication à chercher" style="display:inline-block"/><input type="submit" align="right" class = "btn-primary" name="search_submit" value="Rechercher" style="display:inline-block"/>
                  
                </form>
                </div>
                </center>

          </header>

          <?php  
      // Recherche
      if (isset($_POST) && isset($_POST["recherche"])){
        $contenu = $_POST["recherche"];
        
        try  {                
          $bdd = new PDO('mysql:host=localhost;dbname=dros;charset=utf8', 'root', '');
          // ########################### Recherche par titre ################################################ //    
          $reponse1 = $bdd->query('SELECT * FROM publication WHERE titre = "'.$contenu.'"');
          // ############################## Recherche sur les mots clefs ###################################//
          $keywords = array();
          // si les mots clefs sont séparés par des virgules
          $reponse2 = array();
          $reponse = null;
          if (strpos($contenu, ",")){
            $keywords = explode(",", $contenu);
            foreach ($keywords as $value) {
             $reponse2[$value] = $reponse1 = $bdd->query('SELECT * FROM publication WHERE INSTR (mots_cles, "'.$value.'") != 0');
           }
          
          } else {
            $reponse = $bdd->query('SELECT * FROM publication WHERE INSTR (mots_cles, "'.$contenu.'") != 0');
        }
           
          // ######################################### recherche d'un auteur/chercheur ######################### //
          $reponse3 = $bdd->query('SELECT * FROM publication WHERE INSTR(auteurs, "'.$contenu.'")');

          // affichage des résultats 
          // par titre 
          echo "<p style='color:white;'> <strong> Recherche par titre </strong></p>";
          // /echo count($reponse1);
          while($donnee = $reponse1->fetch()){
            echo "<p style='font-size: 12; color: red'><a href='article.php?titre=".$donnee['titre']."'>". $donnee["titre"]."</a></p>";
          }
          // Par mots clefs
          echo "<p style='color:white;'><strong> recherche par mots clefs </strong></p>";
          foreach ($reponse2 as $key => $value) {
            while($donnee = $value->fetch()){
              echo "<p style='font-size: 12; color: red'><a href='article.php?titre=".$donnee['titre']."'>". $donnee["titre"]."</a></p>";
            }
          }

          if ($reponse != null and $donnee = $reponse->fetch()){
              echo "<p style='font-size: 12; color: red'><a href='article.php?titre=".$donnee['titre']."'>". $donnee["titre"]."</a></p>";
          }

          // pas auteur/chercheur
          echo "<p style='color:white;'> <strong> Recherche par auteur/chercheur </strong></p>";
          if ($reponse3->fetch()){
            echo "<p style='font-size: 12; color: red'><a href='mon-compte.php?username=".$contenu."'>". $contenu."</a></p>";
          }

        }catch (Exception $e) {
          echo ("Erreur ".$e->getMessage());
        } 
      }

      if (isset($_POST['logout']) && $_POST['logout'] == 'Log out'){
        echo "<script type='text/javascript'>document.location.replace('accueil.php');</script>";
      }
    ?>
</h1>        
 <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>

