<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DROS Project</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-casual.min.css" rel="stylesheet">

  </head>
<body>

 <?php include "header.php"; ?>


    <?php 
  $i=0;
  if ($i==0) {
    include("nav-bar.php");
    $i++;
  }
?>
    <section class="page-section clearfix">
      <div class="container">
        <div class="intro">
          <img class="intro-img img-fluid mb-3 mb-lg-0 rounded" src="img/intro.jpg" alt="">
          <div class="intro-text left-0 text-center bg-faded p-5 rounded">
            <h2 class="section-heading mb-4">
              <span class="section-heading-upper">SHANGAI RANKING</span>
              <span class="section-heading-lower">DAUPHINE class�e N�1</span>
            </h2>
            <p class="mb-3">L�universit� Paris-Dauphine est un �tablissement public d�enseignement sup�rieur et de recherche fran�ais sp�cialis� dans les sciences des organisations et des march�s. Elle a �t� Cr��e en 1971 apr�s la scission de l�universit� de Paris avec le nom d�� universit� Paris-IX.
            </p>
            <div class="intro-button mx-auto">
              <a class="btn btn-primary btn-xl" href="#">Visit Us Today!</a>
            </div>
          </div>
        </div>
      </div>
    </section>


    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>




<?php 
	$i=0;
	if ($i==0) {
		include("footer.php");
		$i++;
	}
?>

</body>
</html>