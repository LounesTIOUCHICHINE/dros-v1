<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DROS Project</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-casual.min.css" rel="stylesheet">
</head>
<body>
    <?php include("header.php"); ?>

  <div>
    <?php  include("nav-bar.php");?>
  </div>
  
  <br>
<center>
<div class="body">

	<div class="login-page">
		<div class="row justify-content-md-center">
			<div class="col-md-4">
				<img src="img/logo.png" width="150px" class="user-avatar" />
				<form method="POST">
					<div class="form-content">
						<div class="form-group">
							<input type="text" class="form-control input-underline input-lg" id="xmlPath" placeholder="XML path" name="xmlPath" >
						</div>
						
					</div>
					<div class="log_buttons">
						<button class="btn btn-success" id="load">Load</button>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>

<div class="xmlDiv" id="xmlDiv"></div>

<script>
	var allBooks;
//When button Load is pushed
    document.getElementById("load").onclick = loadXMLFile;
	
	function loadXMLFile(){
		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
			myFunction(this);
			}
		};
		xhttp.open("GET", document.getElementById("xmlPath").value, true);
		xhttp.send();
		return false;
	}
 
	function myFunction(xml) {
		var xmlDoc = xml.responseXML;
		allBooks = xmlDoc.getElementsByTagName("book");
		allTitles = xmlDoc.getElementsByTagName("title");
		allAuthors = xmlDoc.getElementsByTagName("author");
		allPublishers = xmlDoc.getElementsByTagName("publisher");
		allYears = xmlDoc.getElementsByTagName("year");
		allISBN = xmlDoc.getElementsByTagName("isbn");
		
		for (var i=0; i < allBooks.length; i++){
			var dateAjout = allBooks[i].getAttribute('mdate');
			var reference = allBooks[i].getAttribute('key');
			var title = allTitles[i].childNodes[0].nodeValue;
			var author = allAuthors[i].childNodes[0].nodeValue;
			var publisher = allPublishers[i].childNodes[0].nodeValue;
			var publisherlink = allPublishers[i].getAttribute('href');
			var year = allYears[i].childNodes[0].nodeValue;
			var isbn = allISBN[i].childNodes[0].nodeValue;
			
			var s = "<br>Book "+i+" : <br>"+"Date of download : "+dateAjout+"<br> Reference : "+reference+"<br> Title : "+title+"<br> Author : "+author+"<br> Publisher : "+publisher+"<br> Link : "+publisherlink+"<br> Year : "+year+"<br> ISBN : "+isbn+"<br>";
			document.getElementById("xmlDiv").innerHTML += s;	
		}
		
		allCollections = xmlDoc.getElementsByTagName("incollection");
		allTitles = xmlDoc.getElementsByTagName("title");
		allAuthors = xmlDoc.getElementsByTagName("author");
		allPages = xmlDoc.getElementsByTagName("pages");
		allBookTitle = xmlDoc.getElementsByTagName("booktitle");
		allYears = xmlDoc.getElementsByTagName("year");
		allURL = xmlDoc.getElementsByTagName("url");
		
		for (var i=0; i < allCollections.length; i++){
			var dateAjout = allBooks[i].getAttribute('mdate');
			var reference = allBooks[i].getAttribute('key');
			var title = allTitles[i].childNodes[0].nodeValue;
			var author = allAuthors[i].childNodes[0].nodeValue;
			var booktitle = allBookTitle[i].childNodes[0].nodeValue;
			var pages = allPages[i].childNodes[0].nodeValue;
			var year = allYears[i].childNodes[0].nodeValue;
			var url = allURL[i].childNodes[0].nodeValue;
			
			var s = "<br>InCollection "+i+" : <br>"+"Date of download : "+dateAjout+"<br> Reference : "+reference+"<br> Title : "+title+"<br> Author : "+author+"<br> Book TItle : "+booktitle+"<br> Pages : "+pages+"<br> Year : "+year+"<br> URL : "+url+"<br>";
			document.getElementById("xmlDiv").innerHTML += s;	
		}
	}

</script>
 </center>

<?php 
	// consultation d'un article

	if (isset($_GET) && isset($_GET['titre'])){
		$titre = $_GET['titre'];
		$username=$_GET['username'];

		try  {                
          $bdd = new PDO('mysql:host=localhost;dbname=dros;charset=utf8', 'root', '');
          // ########################### Recherche de l'article par son titre ################################################ //    
          $reponse = $bdd->query('SELECT * FROM publication WHERE titre = "'.$titre.'"');

          // affichage des résultats 
        	
        	// titre
          echo "<p style='color:white;'> <strong>".$titre."</strong>";
          // Auteurs
          if($donnee = $reponse->fetch()){
          	echo ", ".$donnee['type_publication']." </p>";
          	echo "<p style='color:white'><strong> Auteurs </strong></p>";
          	$auteurs = explode(",", $donnee['auteurs']);
          	echo "<p style='color:white'>";
          	foreach ($auteurs as $value) {
          		echo "<a href='mon-compte.php?username=".$username."'>".$value."</a>,";
          	}
          	echo "</p>";
          	// Mots clefs
          	 echo "<p style='color:white;'> <strong> Mots clés </strong></p>";
          	 $keywords = $donnee['mots_cles'];
          	 echo "<p style='color:white'>".$keywords."</p>";

          	 // Hypothèses 
          	 $hypotheses = $donnee['hypotheses'];
          	 echo "<p style='color:white;'> <strong>Hypothèses</strong></p>";
          	 echo "<p style='color:white'>".$hypotheses." </p>";
          	 
          	 // Description
          	 $texte = $donnee['texte'];
          	 echo "<p style='color:white;'> <strong>Texte</strong></p>";
          	 echo "<p style='color:white'>".$texte." </p>";
          }else {
          	echo "<p style='color:white'><strong>Cette publication n'est pas encore enregistrée. Si vous voulez l'enregistrer, c'est par <a href='creation_article.php'>ici</a></strong></p>";
          }
        }catch (Exception $e) {
          echo ("Erreur ".$e->getMessage());
        }
	}
?>
 <br>
 
  <?php include("footer.php"); ?>


</body>
</html>




