<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DROS Project</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-casual.min.css" rel="stylesheet">  
</head>
<body>
    <?php 
      include("header.php"); 
      include("nav-bar.php");
    ?>
<center>
  <div class="body">

  <div id="box" class="center">
   <br>
    <form method="POST">
      <select name="type">
        <option value="article">Article</option>
        <option value="experience">Experience</option>
      </select>
      <p></p>
      <input type="text" name="title" placeholder="Publication title" />
      <input type="text" name="authors" placeholder="authors" /> 
      <input type="text" name="keywords" placeholder="Key words" />
      <input type="text" name="hypotheses" placeholder="hypotheses" />
      <input type="text" name="description" placeholder="Description" />
      <input type="text" name="text" placeholder="Publication text" />

      <input class="btn-primary" type="submit" name="creation_submit" value="Publier"/>
    </form>
  </div>

</div>
</center>
<br>

  <?php include "footer.php"; ?>


  <!-- gestion de l'inscription -->
  <?php

  if (isset($_POST)){
    if (isset($_POST["title"]) && isset($_POST["description"]) && isset($_POST["text"]) && isset($_POST["authors"])) {
      // Alors on essaye de connecter l'utilisateur
      $type = $_POST["type"];
      $title = $_POST["title"];
      $authors = $_POST["authors"];
      $keywords = "";
      if (isset($_POST["keywords"]))   $keywords = $_POST["keywords"];

       $hypotheses = "";
      if (isset($_POST["hypotheses"]))   $hypotheses = $_POST["hypotheses"];
      
      $description = $_POST["description"];
      $text = $_POST["text"];

      try
      {
        $bdd = new PDO('mysql:host=localhost;dbname=dros;charset=utf8', 'root', '');
        // vérification de l'existance de la publication dans la bdd
        $reponse = $bdd->query('SELECT * FROM publication WHERE titre = "'.$title.'"');
        
        if ($reponse->fetch() == FALSE){
           // On inscrit l'utilisateur avant de le connecter à son nouveau compte
            $req = $bdd->prepare('INSERT INTO publication(type_publication, titre, mots_cles, auteurs, hypotheses, description, texte) VALUES(:type, :titre, :mots_cles, :auteurs, :hypotheses, :description, :texte)');
            if ($req != FALSE){
              $data = array('type' => $type, 'titre' => $title, 'mots_cles' => $keywords,'auteurs' => $authors,'hypotheses' => $hypotheses, 'description' => $description, 'texte' => $text);
              if ($req->execute($data) == FALSE) print_r( $req->errorInfo());
                echo "<P style='color:green; font-size:20px'> Votre publication a été ajoutée</p>";
                // On ajoute l'article à tous les auteurs 
                /*
                ############### Cette partie n'est pas encore testée ##############
                $auteurs = explode(",", $authors);
                foreach ($auteurs as $value) {
                  $nom = explode (" ", $value); 
                  $reponse1 = $bdd->query('SELECT * FROM user WHERE (prenom="'.$nom[0].'" AND nom="'.$nom[1].'") OR (nom="'.$nom[0].'" AND prenom="'.$nom[1].'")');
                  $user=$reponse->fetch();
                  $username = $user['username'];
                  $publications = "".$user['publications'].", ".$title;
                  $sql = 'UPDATE user set publications="'.$publications.'" where username="'.$username.'"';
                  $reponse = bdd->query($req);
                  
                }*/
            }else {
              echo "erreur non gérée";
            }
        }else {
          echo "<p style='color:red; font-size:20px'> Une publication portant le titre ". $reponse->fetch()['titre']." existe déjà </p>";
          }
        // Connection de l'utilidateur
      }catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
      }      
      }
      
    }

  ?>

</body>
</html>








