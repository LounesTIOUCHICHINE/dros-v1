<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Business Casual - Start Bootstrap Theme</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-casual.min.css" rel="stylesheet">
</head>
<body>
    <?php include("header.php"); ?>

  <div>
    <?php  include("nav-bar.php");?>
  </div>
  
  <br>
<center>
<div class="body">

    <div class="login-page">
        <div class="row justify-content-md-center">
            <div class="col-md-4">
                <img src="img/logo.png" width="150px" class="user-avatar" />
                <form method="POST">
                    <div class="form-content">
                        <div class="form-group">
                            <input type="text" ng-model="name" class="form-control input-underline input-lg"  placeholder="Username" name="username" >
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control input-underline input-lg"  placeholder="Password" name="password" >
                        </div>
                    </div>
                    <div class="log_buttons">
                        <button class="btn btn-success" type="submit"> Log in </button>
                        <a class="btn btn-success" href="signup.php">Register</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
 </center>
 <br>
 
  <?php include("footer.php"); ?>

<?php

  if (isset($_POST)){
    if (isset($_POST["username"]) && isset($_POST["password"])){
      // Alors on essaye de connecter l'utilisateur
      try
      {
        $bdd = new PDO('mysql:host=localhost;dbname=dros;charset=utf8', 'root', '');
        $username = $_POST['username'];
        $password = $_POST["password"];
        $reponse = $bdd->query('SELECT * FROM user where username="'.$username.'" AND password="'.md5($password).'"');

        if ($reponse->fetch()){
          $_GET['username'] = $username;
          echo "<script type='text/javascript'>document.location.replace('mon-compte.php?username=".$username."');</script>";
          $connecter = true;
        }

      }
        catch (Exception $e)
      {
        die('Erreur : ' . $e->getMessage());
      }      
      }
    }

  ?>

</body>
</html>




