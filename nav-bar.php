<!DOCTYPE html>
<html>

<body>
  

<?php 
  if (isset($_GET) && isset($_GET['username'])){
    $usr = $_GET['username'];
    echo '
      <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
  <div class="collapse navbar-collapse container" id="navbarResponsive">
    <ul id="menu-deroulant" class="navbar-nav mx-auto">
      <li class="nav-item px-lg-4">
        <a class="nav-link text-uppercase text-expanded" href="accueil.php?username='.$usr.'">Home</a>
      </li>
      <li class="nav-item px-lg-4">
        <a class="nav-link text-uppercase text-expanded" href="#">Publications</a>
    <ul>
      <li class="nav-item px-lg-4"><a class="nav-link text-uppercase text-expanded" href="article.php?username='.$usr.'">Lire publications XML</a></li>
      <li class="nav-item px-lg-4"><a class="nav-link text-uppercase text-expanded" href="creation_article.php?username='.$usr.'">Créer article</a></li>
    </ul>
      </li>
      <li class="nav-item px-lg-4" >
        <a class="nav-link text-uppercase text-expanded" href="mon-compte.php?username='.$usr.'">Account</a>
      </li>
      <li class="nav-item px-lg-4" >
        <a class="nav-link text-uppercase text-expanded" href="accueil.php?username='.$usr.'"><i class="fal fa-sign-in"></i></a>
      </li>
    </ul>
  </div>
</nav>
    ';
  } else {
    echo '
      <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
  <div class="collapse navbar-collapse container" id="navbarResponsive">
    <ul id="menu-deroulant" class="navbar-nav mx-auto">
      <li class="nav-item px-lg-4">
        <a class="nav-link text-uppercase text-expanded" href="accueil.php">Home</a>
      </li>
      <li class="nav-item px-lg-4">
        <a class="nav-link text-uppercase text-expanded" href="signup.php">Sign Up</a>
      </li>
      <li class="nav-item px-lg-4">
        <a class="nav-link text-uppercase text-expanded" href="login.php">Login
          <!--<span class="sr-only">(current)</span>-->
        </a>
      </li>
      <li class="nav-item px-lg-4">
        <a class="nav-link text-uppercase text-expanded" href="#">Publications</a>
    <ul>
      <li class="nav-item px-lg-4"><a class="nav-link text-uppercase text-expanded" href="article.php">Lire publications XML</a></li>
      <li class="nav-item px-lg-4"><a class="nav-link text-uppercase text-expanded" href="creation_article.php">Créer article</a></li>
    </ul>
      </li>
      <li class="nav-item px-lg-4" >
        <a class="nav-link text-uppercase text-expanded" href="accueil.php"><i class="fal fa-sign-in"></i></a>
      </li>
    </ul>
  </div>
</nav>
    ';
  }
?>
</body>
</html>

 
 