window.onload = function()
{        

    var allBricks;
    var allWalls;
	
    
	//When button Load is pushed
    document.getElementById("load").onclick = loadXMLFile;
     
	
	//Function that reads the XML level file 
    function loadXMLFile(){
		
        var path = document.getElementById("xmlPath").value;
        var xhttp = new XMLHttpRequest();
	
        xhttp.onreadystatechange = function() {
			
            if (this.readyState == 4 && this.status == 200) {
                readXMLFile(this);
            }
        };
        
		xhttp.open("GET", path, true);
        xhttp.send();
		
		
    }

    //Function that convert the XML file to bricks & walls on the canvas 
    function readXMLFile(xml){
        var xmlDoc = xml.responseXML;
        var s = new XMLSerializer();
		var str = s.serializeToString(xmlDoc);
		alert(str);
    }
	
}

