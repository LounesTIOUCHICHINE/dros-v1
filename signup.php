<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Business Casual - Start Bootstrap Theme</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-casual.min.css" rel="stylesheet">  
</head>
<body>
    <?php 
      include("header.php"); 
      include("nav-bar.php");
    ?>

	 <center>
  <div class="body">
  <div id="box" class="center">
	<br>
    <form method="POST">
      <input type="text" id = "name" name="name" placeholder="Nom" />
      <input type="text" name="surname" placeholder="Prénom" />
      <input type="text" name="username" placeholder="Username" />
      <input type="text" name="email" placeholder="E-mail" />
      <input type="password" name="password1" placeholder="Password" />
      <input type="password" name="password2" placeholder="Retype password"/>
      <input class="btn-primary" type="submit" name="signup_submit" value="Sign me up"/>
	  <p class="text-faded"> Already a member ?<a href="login.php"> Go and login </a>
    </form>
  </div>

</div>
  </center>
  <br>
  <?php include "footer.php"; ?>


  <!-- gestion de l'inscription -->
  <?php

  if (isset($_POST)){
    if (isset($_POST["username"]) && isset($_POST["password1"]) && isset($_POST["password2"]) && isset($_POST["email"])) {
      // Alors on essaye de connecter l'utilisateur
      $username = $_POST["username"];
      $password1 = $_POST["password1"];
      $password2 = $_POST["password2"];
      $email = $_POST["email"];
      $name = $_POST["name"];
      $surname = $_POST["surname"];

      // Vérification de l'email
      if (filter_var($email, FILTER_VALIDATE_EMAIL)){
        // Vérification du mot de passe
        if ($password1 == $password2){
          try
          {
            $bdd = new PDO('mysql:host=localhost;dbname=dros;charset=utf8', 'root', '');
            $reponse = $bdd->query('SELECT * FROM user WHERE username = "'.$username.'"');
          
            if ($reponse->fetch() == FALSE){
               // On inscrit l'utilisateur avant de le connecter à son nouveau compte
                $req = $bdd->prepare('INSERT INTO user(nom, prenom, mail, username, password) VALUES(:nom, :prenom, :mail, :username, :password)');
                
                if ($req != FALSE){
                  $data = array('nom' => $name, 'prenom' => $surname, 'mail' => $email,'username' => $username,'password' => md5($password1));
                  if ($req->execute($data) == FALSE) print_r( $req->errorInfo());
                 
                  echo "<script type='text/javascript'>document.location.replace('info_chercheur.php?username=".$username."');</script>";
                 
                }else {
                  echo "erreur non gérée";
                }
            }else {
              echo "<p style='color:red; font-size:20px'> le nom d'utilisateur ". $reponse->fetch()['mail']." existe déjà </p>";
              }
            // Connection de l'utilidateur
          }catch (Exception $e){
            die('Erreur : ' . $e->getMessage());
          }      
        }else {
          echo "<P style='color:red; font-size:20px'> Les deux mots de passe ne sont pas identiques</p>";
        }
      }else {
        echo "<p style='color:red; font-size:20px'> email non valide</p>";
      }
      }
      
    }

  ?>

</body>
</html>








